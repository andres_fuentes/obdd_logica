#include <stdio.h>
#include <stdlib.h>
#include "utilities/Structures.h"
//gcc Proyecto.c utilities/*.c -Iutilities -o Proyecto.exe && Proyecto.exe

int main(int argv, char* args[]){

    /* El programa solo permite variables de la a a la z en minusculas 
       los operadores son
       ! = NOT
       & = AND
       ^ = XOR
       | = OR
    */
    char* expr1 = "(a|b|c)";
    printf("Exrpesion 1: %s\n", expr1);
    Obdd* obdd1 = expr2bdd(expr1);
    printf("Tabla 1:\n");
    bdd2tabla(obdd1);

    char* expr2 = "(a|b|d)";
    printf("Exrpesion 2: %s\n", expr2);
    Obdd* obdd2 = expr2bdd(expr2);
    printf("Tabla 2 sin negar:\n");
    bdd2tabla(obdd2);

/*    Obdd* obdd2_negado = notObdd(obdd2);
    printf("Tabla 2 negada:\n");
    bdd2tabla(obdd2_negado);

    Obdd* obdd3 = apply(XOR,obdd1,obdd2_negado);
    reduce(obdd3);
    printf("Tabla 3 al aplicar la operacion AND a la expresion 1 y expresion 2 negada:\n");
    bdd2tabla(obdd3);*/

    Obdd* obdd4 = apply(AND,obdd1,obdd2);
    reduce(obdd4);
    printf("Tabla 4 al aplicar la operacion Xor a la expresion 1 y expresion 2:\n");
    bdd2tabla(obdd4);


    destructObdd(obdd1);
    destructObdd(obdd2);
    //destructObdd(obdd3);
    destructObdd(obdd4);
}