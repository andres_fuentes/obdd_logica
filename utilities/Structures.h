#ifndef _STRUCTURES_
    #include "Obdd.h"
    #define _STRUCTURES_

    struct ListNode{
        ObddNode* val;
        struct ListNode* next;
    };

    typedef struct ListNode List;

    typedef struct {
        List** array;
        int size;
    } HashMap;

    struct Expression
    {
        bool isUnary;
        char operand;
        struct Expression* right;
        struct Expression* left;
    };

    typedef struct Expression ParseTree;
    
    HashMap* createHashMap(int size);
    int put(ObddNode* element, HashMap* map);
    void deleteMap(HashMap* map);
#endif