#ifndef _OBDD_
    #define _OBDD_
    #include <stdio.h>
    #include <stdlib.h>
    #include <stdbool.h>
    #include <string.h>
    #define getIndex(i, j,columnSize)  ( j + i*columnSize)

    struct _ObddNode
    {
        unsigned int uniqueId;
        unsigned int variable;
        struct _ObddNode* positive;
        struct _ObddNode* negative;
    };

    typedef struct _ObddNode ObddNode;
    
    typedef struct
    {
        ObddNode* raiz;
        int niveles;
        bool isReduced;
        char* nombres;
    } Obdd;

    ObddNode* TRUE();
    ObddNode* FALSE();

    Obdd* reduce(Obdd* a);
    Obdd* apply(bool (*f)(bool,bool), Obdd* a, Obdd* b);
    void bdd2tabla(Obdd* arbol);
    Obdd* expr2bdd(char* expresion);
    Obdd* notObdd(Obdd* a);

    bool eq(ObddNode* a,ObddNode* b);
    Obdd* tabla2bdd(bool* truthTable, int numVariables, char* nombres);
    void destructObdd(Obdd* arbol);
    bool andOp(bool a, bool b);
    bool orOp(bool a, bool b);
    bool xorOp(bool a, bool b);
    bool norOp(bool a, bool b);
    int tamanioArbol(Obdd* arbol);


    void printTree(Obdd* arbol);

    #define AND andOp
    #define OR orOp
    #define XOR xorOp
    #define NOR norOp
#endif