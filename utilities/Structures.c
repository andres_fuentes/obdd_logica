#include "Structures.h"

HashMap* createHashMap(int size){
    HashMap* map = malloc(sizeof(HashMap));
    map->array = calloc(size,sizeof(List*));
    map->size = size;
    return map;
}

int put(ObddNode* element, HashMap* map){
    int index = (element->positive->uniqueId + 10*element->negative->uniqueId)%(map->size);
    //printf("%d\t",index);
    if(map->array[index] != NULL){
        List* lista = map->array[index];
        List* currentNode = lista;
        List* ultimoNodo = currentNode;
        while(currentNode != NULL){
            if(eq(currentNode->val, element)){
                return currentNode->val->uniqueId; // termina el programa porque esoty intentando agregar un elemento que ya existe
            } else{
                ultimoNodo = currentNode;
                currentNode = currentNode->next;
            }
        }
        List* newNode = malloc(sizeof(List));
        newNode->val = element;
        newNode->next = NULL;
        ultimoNodo->next = newNode;
        return -1;
    }
    List* newNode = malloc(sizeof(List));
    newNode->val = element;
    newNode->next = NULL;
    map->array[index] = newNode;
    return -1;
}

void deleteMap(HashMap* map){
    for(int i=0;i < map->size; i++){
        if(map->array[i] != NULL){
            List* currentNode = map->array[i];
            while (currentNode != NULL)
            {
                List* temp = currentNode;
                currentNode = currentNode->next;
                free(temp);
            }
            
        }
    }
    free(map->array);
    free(map);
}