#include "Structures.h"
static ObddNode _TRUE = {1, 0, NULL, NULL};
static ObddNode _FALSE = {0, 0,NULL, NULL};
static ObddNode* applyNode(bool (*f)(bool,bool), ObddNode* a, ObddNode* b, int nivel, int id, int columnSize, int* depth, char* nomA, char* nomB, char* nombresPosibles);
//static bool evaluateParse(ParseTree* tree,int variables[]);
Obdd* evaluateParseToObbd(ParseTree* tree);
//static void printParseTree(ParseTree* tree); //Funcion para debuggear el codigo de parseo
static void destructParseTree(ParseTree* tree); //Funcion para debuggear el codigo de parseo
static void printNode(ObddNode* node);

ObddNode* TRUE(){
    return &_TRUE;
}

ObddNode* FALSE(){
    return &_FALSE;
}

Obdd* reduce(Obdd* a){
    int niveles = a->niveles;
    int nextLabel = 2; //En teoria los nodos 0 y 1 siempre tienen el label 0 y 1
    int totalNodos = (1 << niveles) - 1 ;
    ObddNode** stack = calloc(totalNodos, sizeof(ObddNode*));
    if(stack == NULL){
        fprintf(stderr, "No memoria para el stack\n");
        exit(EXIT_FAILURE);
    }
    // Pongo los nodos en un stack para poder recorrerlos de abajo hacia arriba como lo requiere el alogritmo de reduce
    int stackIndex = totalNodos;
    int currentElement = totalNodos;
    ObddNode* currentNode = NULL; 
    stack[--stackIndex] = a->raiz;
    while (stackIndex >=0 && currentElement > stackIndex)
    {
        currentNode = stack[--currentElement];

        if(currentNode->positive != TRUE()  && currentNode->positive != FALSE()){
            stack[--stackIndex] = currentNode->positive;
        }

        if(currentNode->negative != TRUE()  && currentNode->negative != FALSE()){
            stack[--stackIndex] = currentNode->negative;
        }
    }

    /* Marco los nodos del obdd con labels, siguiendo las siguientes reglas
    *  1.- Si el nodo es TRUE la label es 1, si es FALSE es 0 
    *  2.- si id(lo(n)) = id(hi(n)) entonce id(n) sera id(lo(n))
    *  3.- si var(m) = var(n), lo(m) = lo(n) y hi(m) = hi(n) entonces  id(n) = id(m)
    */
    int backupIndex = stackIndex;
    int lengthSearchArrays = niveles+2;
    HashMap* map = createHashMap(lengthSearchArrays);
    ObddNode** labels = calloc(totalNodos+2,sizeof(ObddNode*));
    if(labels == NULL){
        fprintf(stderr, "No memoria para el array de etiquietas\n");
        exit(EXIT_FAILURE);
    }
    labels[0] = FALSE();
    labels[1] = TRUE();
    for(; stackIndex < totalNodos; stackIndex++){
        currentNode = stack[stackIndex];
        /*printNode(currentNode);
        printf(" \t");*/
        int labelPositiveChild = currentNode->positive->uniqueId;
        int labelNegativeChild = currentNode->negative->uniqueId;
        if(labelPositiveChild == labelNegativeChild){
            currentNode->uniqueId = labelNegativeChild;
        } else {
            int existingLabel = put(currentNode, map);
            if(existingLabel < 0){
                currentNode->uniqueId = nextLabel;
                labels[nextLabel] = currentNode;
                nextLabel++;
            } else {
                currentNode->uniqueId = existingLabel;
            }
        }
       /*printNode(currentNode);
        printf("\n");*/

    }
    
    // Una vez marcados los nodos se procede a reducir el obdd
    if(a->raiz->uniqueId == 0 || a->raiz->uniqueId == 1){
        int value = a->raiz->uniqueId;
        for(stackIndex=backupIndex; stackIndex < totalNodos; stackIndex++){
            free(stack[stackIndex]);
        }
        a->raiz = value? TRUE() : FALSE();
        a->niveles = 0;
    } else {
        currentNode = NULL;
        bool* used = calloc(totalNodos+2, sizeof(bool)); 
        for(stackIndex=backupIndex; stackIndex < totalNodos; stackIndex++)
        {
            currentNode = stack[stackIndex];
            ObddNode* newPositiveChild = labels[currentNode->positive->uniqueId];
            ObddNode* newNegativeChild = labels[currentNode->negative->uniqueId];
            used[currentNode->positive->uniqueId] = true;
            used[currentNode->negative->uniqueId] = true;
            if(newPositiveChild != currentNode->positive){
                currentNode->positive = newPositiveChild;
            }

            if(newNegativeChild != currentNode->negative){
               currentNode->negative = newNegativeChild; 
            }

        }
        for(int i=0; i < totalNodos + 2; i++){
            ObddNode* nodo = labels[i];
            if(nodo!= NULL && !used[i] && i != a->raiz->uniqueId){
                free(labels[i]);
            } 
        }
        //Esta parte del codigo borra las variables que no se usaron
        bool* variables = calloc(a->niveles, sizeof(bool));
        for(int j=0; j <a->niveles; j++){
            variables[j] = false;
        }
        stackIndex = totalNodos;
        currentElement = totalNodos;
        currentNode = NULL; 
        stack[--stackIndex] = a->raiz;
        while (stackIndex >=0 && currentElement > stackIndex)
        {
            currentNode = stack[--currentElement];

            if(currentNode->positive != TRUE()  && currentNode->positive != FALSE()){
                stack[--stackIndex] = currentNode->positive;
            }

            if(currentNode->negative != TRUE()  && currentNode->negative != FALSE()){
                stack[--stackIndex] = currentNode->negative;
            }
        }
        int backupIndex = stackIndex;
        for(; stackIndex < totalNodos; stackIndex++){
            currentNode = stack[stackIndex];
            variables[currentNode->variable] = true;
        }
        int newNumVariables = 0;
        for(int j=0; j <a->niveles; j++){
            if(variables[j]){
                newNumVariables++;
            }
        }
        char* nombres = calloc(newNumVariables,sizeof(char));
        int index = 0;
        for(int j=0; j < a->niveles; j++){
            if(variables[j]){
               nombres[index++] = a->nombres[j];
            }
        }
        for(stackIndex=backupIndex; stackIndex < totalNodos; stackIndex++){
            currentNode = stack[stackIndex];
            for(int j=0; j < newNumVariables; j++){
                if(a->nombres[currentNode->variable] == nombres[j]){
                    currentNode->variable = j;
                }
            }
        }
        free(a->nombres);
        a->nombres = nombres;
        a->niveles = newNumVariables;
    } 

    free(stack);
    free(labels);
    deleteMap(map);

    return a;
}

Obdd* apply(bool (*f)(bool,bool), Obdd* a, Obdd* b){

    //int  numNodosA = a->raiz->uniqueId + 2;
    int  numNodosB = b->raiz->uniqueId + 2;
    int depth = 1;
    char* nombresPosibles = calloc(26,sizeof(char));
    ObddNode* nuevaRaiz = applyNode(f, a->raiz, b->raiz, 0, 2, numNodosB, &depth, a->nombres,b->nombres,nombresPosibles);
    Obdd* result = malloc(sizeof(Obdd));
    if(result == NULL){
        fprintf(stderr, "No memoria para el obbd despues del apply\n");
        exit(EXIT_FAILURE);
    }
    result->raiz = nuevaRaiz;
    result->niveles = depth;
    result->isReduced = false;
    char* nombres = calloc(depth,sizeof(char));
    for(int i=0;i<depth;i++){

        nombres[i] = nombresPosibles[i];
    }
    result->nombres = nombres;
    return result;
}

static ObddNode* applyNode(bool (*f)(bool,bool), ObddNode* a, ObddNode* b, int nivel, int id, int columnSize, int* depth, char* nomA, char* nomB, char* nombresPosibles){
    ObddNode*raiz;
    if((a == TRUE() || a == FALSE()) && (b == TRUE() || b == FALSE())){
        bool valueA = a == TRUE()? true : false;
        bool valueB = b == TRUE()? true : false;
        bool result = f(valueA,valueB);
        raiz = result ? TRUE() : FALSE();
        return raiz;
    } else if((b == TRUE() || b == FALSE()) || (nomB[b->variable] > nomA[a->variable] && !(a == TRUE() || a == FALSE()))){
        raiz = malloc(sizeof(ObddNode));
        if(raiz == NULL){
            fprintf(stderr, "No hay memoria para el nuevo obdd\n");
            exit(EXIT_FAILURE);
        }

        nombresPosibles[nivel] = nomA[a->variable];
        raiz->variable = nivel++;
        raiz->uniqueId = id;
        raiz->negative = applyNode(f, a->negative, b, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
        raiz->positive = applyNode(f, a->positive, b, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
    } else if((a == TRUE() || a == FALSE()) || (nomA[a->variable] > nomB[b->variable] && !(b == TRUE() || b == FALSE()))) {
        raiz = malloc(sizeof(ObddNode));
        if(raiz == NULL){
            fprintf(stderr, "No hay memoria para el nuevo obdd\n");
            exit(EXIT_FAILURE);
        }
        nombresPosibles[nivel] = nomB[b->variable];
        raiz->variable = nivel++;
        raiz->uniqueId = id;
        raiz->negative = applyNode(f, a, b->negative, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
        raiz->positive = applyNode(f, a, b->positive, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
    } else {
        raiz = malloc(sizeof(ObddNode));
        if(raiz == NULL){
            fprintf(stderr, "No hay memoria para el nuevo obdd\n");
            exit(EXIT_FAILURE);
        }
        nombresPosibles[nivel] = nomA[a->variable];
        raiz->variable = nivel++;
        raiz->uniqueId = id;
        raiz->negative = applyNode(f, a->negative, b->negative, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
        raiz->positive = applyNode(f, a->positive, b->positive, nivel, ++id, columnSize, depth, nomA, nomB, nombresPosibles);
    }
    *depth = nivel > *depth? nivel : *depth;
    return raiz;

}

bool eq(ObddNode* a,ObddNode* b){
    return  a->positive->uniqueId == b->positive->uniqueId && a->negative->uniqueId == b->negative->uniqueId;
}

void printTree(Obdd* arbol){
    if(arbol == NULL){
        fprintf(stderr, "\nNo se puede imprimir un arbol nulo\n");
        exit(EXIT_FAILURE);
    }
    ObddNode* raiz = arbol->raiz;
    printNode(raiz);
    printf("\n");    
    return;
}

void printNode(ObddNode* nodo){
    if(nodo == NULL){
        fprintf(stderr, "\nNo se puede imprimir un nodo nulo\n");
        exit(EXIT_FAILURE);
    }
    if(nodo == TRUE()){
        printf("TRUE");
        return;
    } else if(nodo == FALSE()){
        printf("FALSE");
        return;
    } else {
        char nombre = 'a' + nodo->variable;
        printf("%c_%d (",nombre, nodo->uniqueId);
        printNode(nodo->negative);
        printf(" , ");
        printNode(nodo->positive);
        printf(" )");
        return;
    }
}

void destructObdd(Obdd* arbol){
    if(arbol->raiz == TRUE() || arbol->raiz == FALSE()){
        return;
    }
    int totalNodos = (1<<arbol->niveles) -1;
    ObddNode** stack = calloc(totalNodos, sizeof(ObddNode*));
    if(stack == NULL){
        fprintf(stderr, "No hay memoria para crear stack\n");
        exit(EXIT_FAILURE);
    }
    int stackIndex = totalNodos;
    int currentElement = totalNodos;
    ObddNode* currentNode = NULL; 
    stack[--stackIndex] = arbol->raiz;
    while (stackIndex >=0 && currentElement > stackIndex)
    {
        currentNode = stack[--currentElement];

        if(currentNode->positive != TRUE()  && currentNode->positive != FALSE()){
            stack[--stackIndex] = currentNode->positive;
        }

        if(currentNode->negative != TRUE()  && currentNode->negative != FALSE()){
            stack[--stackIndex] = currentNode->negative;
        }
    }

    for(;stackIndex < totalNodos; stackIndex++){
        free(stack[stackIndex]);
    }
    free(stack);
    free(arbol->nombres);
    free(arbol);
}

void bdd2tabla(Obdd* arbol){
    int numVariables = arbol->niveles;

    if(arbol->raiz == TRUE()){
        printf("La expresion Verdadero\n");
        return;
    } else if(arbol->raiz == FALSE()) {
        printf("La expresion Falso\n");
        return;
    }

    int numRows = 1 << numVariables;
    bool* truthTable = malloc(sizeof(bool)*(numRows*(numVariables+1)));

    for(int i=0; i < numVariables+1; i++){
        if(i==numVariables){
            printf("|\tR\n");
        } else {
            char nombre = arbol->nombres[i];
            printf("%c\t", nombre);
        }
    }

    for(int i=0; i < numVariables+1; i++){
        if(i==numVariables){
            printf("|\t\n");
        } else {
            printf("\t");
        }
    }

    int count = 0;
    for(int i=0; i < numRows; i++){
        count = i;
        for(int j = numVariables-1; j >= 0; j--){
            int temp = count;
            truthTable[getIndex(i,j,(numVariables+1))] = count%2;
            count = temp/2;
        }     

        ObddNode* current = arbol->raiz;
        for(int j = 0; j < numVariables; j++){
            bool value = truthTable[getIndex(i,j,(numVariables+1))];
            if(current->variable == j){
                if(value){
                    current = current->positive;
                }else{
                    current = current->negative;
                }
            }
            printf("%d\t",value);
        }      
        if(current == TRUE()){
            printf("|\t1\n");
        } else if(current == FALSE()){
            printf("|\t0\n");
        } else {
            fprintf(stderr, "Error: el OBDD es invalido\n");
            exit(EXIT_FAILURE);
        }
    }
    printf("\n\n");
}


Obdd* expr2bdd(char* expresion){
    int index = 0;
    char current;
    int state = 0;

    ParseTree* raiz = malloc(sizeof(ParseTree));
    raiz->operand = '-';
    raiz->left = NULL;
    raiz->right = NULL;
    raiz->isUnary = false;
    ParseTree* currentExpression = raiz;
    int size = 1000;
    ParseTree** stack = calloc(size, sizeof(ParseTree*));
    int stackPointer = size -1;
    int variables[26];
    for(int j=0; j <26; j++){
        variables[j] = -1;
    }
    int openBrackets = 0;
    while((current=expresion[index]) != '\0'){
        index++;
        if(current == ' '){
            continue;
        } else {
            switch (state)
            {
            case 0: 
            {
                if(current == '!'){
                    currentExpression->isUnary = true;
                    currentExpression->operand = '!';
                    currentExpression->left = malloc(sizeof(ParseTree));
                    currentExpression = currentExpression->left;
                } else if(current == '(') {
                    openBrackets++;
                    stack[stackPointer--] = raiz;
                    raiz = malloc(sizeof(ParseTree));
                    raiz->operand = '-';
                    raiz->left = NULL;
                    raiz->right = NULL;
                    raiz->isUnary = false;
                    currentExpression = raiz;
                } else if( 'a' <= current && current <= 'z'){
                    variables[current - 'a'] = 0;
                    currentExpression->isUnary = true;
                    currentExpression->operand = current;
                    if(raiz->operand != '-'){
                        currentExpression = raiz;
                    }
                    state = 1;
                } else {
                    fprintf(stderr, "Expresion invalida\n");
                    exit(EXIT_FAILURE);
                }
                break;
            }
            case 1:
            {
                if(current == '|' || current == '&' || current == '^'){
                    ParseTree* tree = malloc(sizeof(ParseTree));
                    tree->left = currentExpression;
                    tree->operand = current;
                    tree->isUnary = false;
                    tree->right = malloc(sizeof(ParseTree));
                    currentExpression = tree->right;
                    raiz = tree;
                    state = 0;
                } else if(current == ')'){
                    openBrackets--;
                    if(openBrackets < 0){
                        fprintf(stderr, "Parentesis desbalanceados\n");
                        exit(EXIT_FAILURE);
                    }
                    ParseTree* temp = stack[++stackPointer];

                    if(temp->operand != '-' && !temp->isUnary){
                        if(temp->right != NULL && temp->right->operand == '!'){
                            temp->right->left = raiz;
                        } else {
                            temp->right = raiz;
                        }
                        raiz = temp;
                        currentExpression = raiz;
                    } else if(temp->isUnary){
                        temp->left = raiz;
                        raiz = temp;
                        currentExpression = raiz;
                    } else {

                        free(temp);
                    }

                } else {
                    fprintf(stderr, "Expresion invalida\n");
                    exit(EXIT_FAILURE);
                }
                break;
            }
            case 2:
            {
                break;
            }
            default:
                break;
            }
        }
    }
    if(state != 1){
        fprintf(stderr, "Expresion invalida\n");
        exit(EXIT_FAILURE);
    } else if(openBrackets != 0){
        fprintf(stderr, "Parentesis desbalanceados\n");
        exit(EXIT_FAILURE);
    }

    int numVariables = 0;
    for(int j=0; j <26; j++){
        if(variables[j] != -1){
            numVariables++;
        }
    }
    char* nombres = calloc(numVariables, sizeof(char));
    index = 0;
    for(int j=0; j <26; j++){
        if(variables[j] != -1){
            nombres[index++] = 'a' + j; 
        }
    }
 /*
    printf("\n");
    int numRows = 1 << numVariables;
    bool* truthTable = malloc(sizeof(bool)*(numRows*(numVariables+1)));
    int count = 0;
    for(int i=0; i < numRows; i++){
        count = i;
        for(int j = numVariables-1; j >= 0; j--){
            int temp = count;
            truthTable[getIndex(i,j,(numVariables+1))] = count%2;
            count = temp/2;
        }

        for(int j = 0; j < numVariables; j++){
            bool value = truthTable[getIndex(i,j,(numVariables+1))];
            variables[nombres[j] - 'a'] = value;
        }      

        truthTable[getIndex(i,numVariables,(numVariables+1))] = evaluateParse(raiz, variables);
    }*/
    Obdd* obdd =  evaluateParseToObbd(raiz);
    destructParseTree(raiz);
    //Obdd* obdd = tabla2bdd(truthTable, numVariables, nombres);
    //free(truthTable);
    free(stack);
    return obdd;
}



void destructParseTree(ParseTree* tree){
    if(tree->isUnary){
        if(tree->operand == '!'){
            destructParseTree(tree->left);
            free(tree);
        } else {
            free(tree);
        }
    } else {
        destructParseTree(tree->left);
        destructParseTree(tree->right);
        free(tree);
    }
}

/*void printParseTree(ParseTree* tree){
    if(tree->isUnary){
        if(tree->operand == '!'){
            printf(" %c ",tree->operand);
            printf("(");
            printParseTree(tree->left);
            printf(")");
        } else {
            printf("%c",tree->operand);
        }
    } else {
        printf("(");
        printParseTree(tree->left);
        printf(")");
        printf(" %c ",tree->operand);
        printf("(");
        printParseTree(tree->right);
        printf(")");
    }
}*/

/*bool evaluateParse(ParseTree* tree,int variables[]){
    if(tree->isUnary){
        if(tree->operand == '!'){
            return !evaluateParse(tree->left, variables);
        } else {
            return variables[tree->operand - 'a'];
        }
    } else {
        if(tree->operand == '|'){
            return (evaluateParse(tree->left, variables) | evaluateParse(tree->right, variables));
        } else if(tree->operand == '&'){
            return (evaluateParse(tree->left, variables) & evaluateParse(tree->right, variables));
        } else if(tree->operand == '^'){
            return (evaluateParse(tree->left, variables) ^ evaluateParse(tree->right, variables));  
        } else {
            fprintf(stderr, "Algo salio mal, operacion invalida %c\n");
            exit(EXIT_FAILURE);
        }
    }
}*/

Obdd* evaluateParseToObbd(ParseTree* tree){
    if(tree->isUnary){
        if(tree->operand == '!'){
            return notObdd(evaluateParseToObbd(tree->left));
        } else {

            ObddNode* raiz = malloc(sizeof(ObddNode));
            if(raiz == NULL){
                fprintf(stderr, "No hay memoria para el nuevo obdd\n");
                exit(EXIT_FAILURE);
            }

            raiz->variable = 0;
            raiz->uniqueId = 2;
            raiz->negative = FALSE();
            raiz->positive = TRUE();

            Obdd* result = malloc(sizeof(Obdd));
            if(result == NULL){
                fprintf(stderr, "No memoria para el obbd despues del apply\n");
                exit(EXIT_FAILURE);
            }
            result->raiz = raiz;
            result->niveles = 1;
            result->isReduced = false;
            result->nombres = calloc(1,sizeof(char*));
            result->nombres[0] = tree->operand;
            return result;
        }
    } else {
        if(tree->operand == '|'){
            return apply(OR, evaluateParseToObbd(tree->left) , evaluateParseToObbd(tree->right));
        } else if(tree->operand == '&'){
            return apply(AND, evaluateParseToObbd(tree->left) , evaluateParseToObbd(tree->right));
        } else if(tree->operand == '^'){
            return apply(XOR, evaluateParseToObbd(tree->left) , evaluateParseToObbd(tree->right));
        } else {
            fprintf(stderr, "Algo salio mal, operacion invalida\n");
            exit(EXIT_FAILURE);
        }
    }
}

Obdd* tabla2bdd(bool* truthTable, int numVariables, char* nombres){
    int numRows = 1 << numVariables;
    int uniqueId = 2;
    Obdd* arbol = malloc(sizeof(Obdd));
    if(arbol == NULL){
        fprintf(stderr, "No hay memoria para el arbol");
        exit(EXIT_FAILURE);
    }
    arbol->niveles = numVariables;
    arbol->isReduced = false;
    arbol->nombres = nombres;
    ObddNode* raiz = malloc(sizeof(ObddNode));
    raiz->negative = NULL;
    raiz->positive = NULL;
    raiz->variable = 0;
    raiz->uniqueId = uniqueId;
    arbol->raiz = raiz;
    ObddNode* current = raiz;
    for(int i = 0; i < numRows; i++){
        for (int j = 0; j < numVariables; j++)
        {
            int value = truthTable[getIndex(i,j,(numVariables+1))]; 
            if(value){
                if(current->positive == NULL && j < (numVariables-1)){
                    ObddNode* temp = malloc(sizeof(ObddNode));
                    if(temp == NULL){
                        fprintf(stderr, "No hay memoria para el nodo");
                        exit(EXIT_FAILURE);
                    }
                    temp->negative = NULL;
                    temp->positive = NULL;
                    temp->variable = j+1;
                    temp->uniqueId = ++uniqueId;
                    current->positive = temp;
                    current = temp;
                } else if(current->positive == NULL){
                    int result = truthTable[getIndex(i,(j+1),(numVariables+1))]; 
                    current->positive = result? TRUE() : FALSE();
                } else {
                    current = current->positive;
                }
            } else {
                if(current->negative == NULL && j < (numVariables-1)){
                    ObddNode* temp = malloc(sizeof(ObddNode));
                    if(temp == NULL){
                        fprintf(stderr, "No hay memoria para el nodo");
                        exit(EXIT_FAILURE);
                    }
                    temp->negative = NULL;
                    temp->positive = NULL;
                    temp->variable = j+1;
                    temp->uniqueId = ++uniqueId;
                    current->negative = temp;
                    current = temp;
                } else if(current->negative == NULL){
                    int result = truthTable[getIndex(i,(j+1),(numVariables+1))]; 
                    current->negative = result? TRUE() : FALSE();
                } else{
                    current = current->negative;
                }
            } 
            
        }
        current = raiz;
    }
    return arbol;
}

int tamanioArbol(Obdd* arbol){
    int size = sizeof(Obdd);
    if(arbol->raiz == TRUE() || arbol->raiz == FALSE()){
        return sizeof(ObddNode);
    }
    int totalNodos = (1<<arbol->niveles) -1;
    ObddNode** stack = calloc(totalNodos, sizeof(ObddNode*));
    if(stack == NULL){
        fprintf(stderr, "No hay memoria para crear stack\n");
        exit(EXIT_FAILURE);
    }
    int stackIndex = totalNodos;
    int currentElement = totalNodos;
    ObddNode* currentNode = NULL; 
    stack[--stackIndex] = arbol->raiz;
    while (stackIndex >=0 && currentElement > stackIndex)
    {
        currentNode = stack[--currentElement];

        if(currentNode->positive != TRUE()  && currentNode->positive != FALSE()){
            stack[--stackIndex] = currentNode->positive;
        }

        if(currentNode->negative != TRUE()  && currentNode->negative != FALSE()){
            stack[--stackIndex] = currentNode->negative;
        }
    }

    for(;stackIndex < totalNodos; stackIndex++){
        size += sizeof(ObddNode);
    }
    free(stack);
    return size;
}

Obdd* notObdd(Obdd* a){
    return apply(NOR,a,a);
}


bool andOp(bool a, bool b){
    return a & b;
}

bool orOp(bool a, bool b){
    return a | b;
}

bool xorOp(bool a, bool b){
    return a ^ b;
}

bool norOp(bool a, bool b){
    return !(a | b);
}